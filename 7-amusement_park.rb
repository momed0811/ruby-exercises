class Attendee
    def initialize(height)
        @height = height
        @pass_id = nil
    end
    def height
        @height
    end
    def pass_id
        @pass_id
    end
    def issue_pass!(pass_id)
        @pass_id = pass_id
    end
    def revoke_pass!
        @pass_id = nil
    end
end
puts attendee1 = Attendee.new(106)
puts "**********************"
puts attendee1 = Attendee.new(106).height
puts "**********************"
puts attendee1 = Attendee.new(106).pass_id
puts "**********************"
puts attendee2 = Attendee.new(106)
puts attendee2.issue_pass!(42)
puts attendee2.pass_id
puts "**********************"
attendee = Attendee.new(106)
attendee.issue_pass!(42)
puts attendee.revoke_pass!
puts attendee.pass_id



