module Diamond

  def self.make_diamond(letter)
    width = letter.ord - 'A'.ord + 1
    top = 'A'.upto(letter).each_with_index.map do |l, idx|
      line = ' ' * width
      line[idx] = l
      line.reverse + line[1..-1] + "\n"
    end

    (top + top.reverse[1..-1]).join
  end
end

puts Diamond.make_diamond('E')
