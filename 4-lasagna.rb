class Lasagna
    LAYER_PREPARATION_TIME = 2
    EXPECTED_MINUTES_IN_OVEN = 40 
    def remaining_minutes_in_oven(actual_minutes_in_oven)
        EXPECTED_MINUTES_IN_OVEN - actual_minutes_in_oven
    end
    def preparation_time_in_minutes(layers)
        LAYER_PREPARATION_TIME * layers
    end
    def total_time_in_minutes(number_of_layers:, actual_minutes_in_oven:)
        preparation_time_in_minutes(number_of_layers) + actual_minutes_in_oven
    end
end

first_meal = Lasagna.new
print "expected minutes in oven : ", Lasagna::EXPECTED_MINUTES_IN_OVEN, "\n"
puts "************************"
second_meal = Lasagna.new
print "remaining minutes in oven : ", second_meal.remaining_minutes_in_oven(30), "\n"
puts "************************"
third_meal = Lasagna.new
print "preparation time in 2 minutes : ", third_meal.preparation_time_in_minutes(2), "\n"
puts "************************"
fourth_meal = Lasagna.new
print "total time in minutes : ", fourth_meal.total_time_in_minutes(number_of_layers: 3, actual_minutes_in_oven: 20)
