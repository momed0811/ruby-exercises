class Microwave  
  def initialize(seconds)
    @seconds = seconds
  end

  def timer
    if @seconds < 100
      hours = @seconds / 60
      minutes = @seconds % 60
    else
      hours = @seconds / 100
      minutes = @seconds % 100
    end
    [hours, minutes].map{|i| '%02d' % i}.join(':')
  end
end

